package nmmu.gamedev.tut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Main class that serves as entry into the program
 */
public class Main {
    public static void main(String[] args) throws Exception {
        SimpleQuery query = new SimpleQuery();
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your City, Country: ");
        String city = input.nextLine();
        query.getData(city);
        ArrayList<Forecast> forecasts = getForecasts(query);
        display(forecasts, city);
    }

    private static ArrayList<Forecast> getForecasts(SimpleQuery query) throws JSONException {
        ArrayList<Forecast> forecasts = new ArrayList<Forecast>();
        JSONObject item = query.getObject("query").getJSONObject("results").getJSONObject("channel").getJSONObject("item");
        try {
            JSONArray forecastsList = item.getJSONArray("forecast");
            for (int i = 0; i < forecastsList.length(); i++) {
                JSONObject temp = forecastsList.getJSONObject(i);
                forecasts.add(new Forecast(
                        temp.getString("code"),
                        temp.getString("date"),
                        temp.getString("day"),
                        Integer.parseInt(temp.getString("high")),
                        Integer.parseInt(temp.getString("low")),
                        temp.getString("text")
                ));
            }
        } catch (JSONException e) {
            // sometimes the weather isn't available for a city -- in that case print the error description
            System.out.print(item.getString("description"));
        }
        return forecasts;
    }

    private static void display(ArrayList<Forecast> forecasts, String city) {
        System.out.println("=================================================================");
        System.out.println("\t\t\t\t\t\t\tCLOUDY");
        System.out.printf("Five day weather forecast for %s\n", city);
        System.out.println("=================================================================");
        for(Forecast forecast : forecasts)
            forecast.display();
        System.out.println("==================== Data from Yahoo Weather ====================");
    }
}
