package nmmu.gamedev.tut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Class that stores all the information gathered from an HTTP request as JSON data
 */
public class SimpleQuery {
    public JSONObject data;

    public SimpleQuery() {
        data = null;
    }

    public void getData(String location) throws Exception {
        // base url from yahoo weather api
        String baseurl = "https://query.yahooapis.com/v1/public/yql?q=";
        // yahoo query for location (format is City, Country)
        String yql_query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + location + "\") and u='c'";
        // create the url
        URL url = new URL(baseurl + URLEncoder.encode(yql_query,"UTF-8") + "&format=json");
        // open a connection to the url
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        // set the request method to be GET
        connection.setRequestMethod("GET");
        // commence connection
        connection.connect();
        // receive response as stream
        InputStream in = connection.getInputStream();
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        // convert the stream to a string
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = streamReader.readLine()) != null)
            responseStrBuilder.append(inputStr);
        // set the json object
        data = new JSONObject(responseStrBuilder.toString());
    }

    public JSONObject getObject(String key) throws JSONException {
        return data.getJSONObject(key);
    }

    public JSONArray getArrayObject(String key) throws JSONException{
        return data.getJSONArray(key);
    }
}
