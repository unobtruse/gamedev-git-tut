package nmmu.gamedev.tut;

/**
 * Created by minnaar on 2015/04/10.
 */
public class Forecast {
    public String code, date, day, text;
    public int high, low;

    public Forecast(String code, String date, String day, int high, int low, String text) {
        this.code = code;
        this.date = date;
        this.day = day;
        this.text = text;
        this.low = low;
        this.high = high;
    }

    public void display() {
        System.out.printf("%s\t%d'C - %d'C\n", day, low, high);
    }
}
