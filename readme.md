#Introduction
In this tutorial we're going to create a make-believe repository and simulate the most common issues you'll run into using Git. I'm using several online tutorials and cheat sheets as a reference. Feel free to check them out in the references list at the bottom of this page. If there are any concepts that you still don't understand after reading about them, just do a quick Google search -- there are many pages dedicated to explaining git concepts. I recommend reading the official [git-scm documentation](http://git-scm.com/docs/) or simply searching for git related topics on the official *git-scm* website by adding the `site:git-scm.com` keyword to your Google searches. 

Let's get started!

#Install a Git client
To use git, you'll need to install a git client first. Follow [this link](http://www.git-scm.com/downloads) to download and install the appropriate Git client for your OS.

**For the remainder of this tutorial we'll be using the Git bash to input any code specified**.  Using a command line interface frees us from doing quite a lot of GUI manipulation, although you should be able to easily use the GUI interface after reading through this tutorial. We'll be using a couple of Unix commands that you won't need to use regularly, so try not to worry about those too much. The only commands you really need to remember are those that follow lines starting with `git`.

For Windows users the Git bash is located in the start menu under *All Programs, Git, Git Bash* by default. 

#Configure your Git environment
Firstly you need to tell Git your name and email address so that the changes you commit have a name and email address linked to them. For this tutorial you can really use any values, but in general you'd configure these settings to reflect the the details matching your remote repository account. For the game-dev group you'll want to configure it to work properly with Assembla which expects the username you selected for your Assembla account and your NMMU email address.

Configure your username by typing the following into Git bash: 
```
#!bash
git config --global user.name "your-assembla-username"
```

Configure your email address by typing: 
```
#!bash
git config --global user.email "your-nmmu-email-address"
```

(Sidebar: If you're working with several remote repositories you can omit the `--global` option to cater for each individual repository's configuration requirements)

#Working with an existing project
We're going to be working on a console based simple weather application called "Cloudy". I've already created the base code for this application, so now we simply have to pull it from a remote repository and start making our changes.

Move to your local directory by typing: 
```
#!bash
cd ~/
```

Clone the existing remote repository to you local machine: 
```
#!bash
git clone https://unobtruse@bitbucket.org/unobtruse/gamedev-git-tut.git
```

You should now have a local version of the "Cloudy" repository located at *c:\users\your-username\gamedev-git-tut*.

#Making changes to your project
Having a local version of the repository means that git will track any changes that you make to the code, when you tell it to do so. Being a skilled programmer, you want to improve the project...

Navigate into the repository in git bash: *(Hint: when you're typing commands in the bash, you can use the tab key to auto-complete)* 
```
#!bash
cd gamedev-git-tut
``` 

Now run check the status of changes in your repository
```
#!bash
git status
```

![No changes](http://i.imgur.com/VvyKiKM.png?1)

Git detects that no changes have been made to the files that it's tracking in the repository. Since you've cloned this repository all the files in the folder *gamedev-git-tut* -- the home of your repo -- are automatically tracked. Later on when you start creating new files, git will ask you whether you'd like to track them or not, but we'll deal with that later. Go ahead and open the project in IntelliJ. Run it to check your weather.

With the project open in IntelliJ go ahead and add the *text* attribute to the display method by changing line 19 of *Forecast.java* to read as follows:
```
#!java
public void display() {System.out.printf("%s\t%d'C - %d'C -- %s\n", day, low, high, text);}
```

Running it now will add the weather conditions to the output.

Once you've made some changes, check the status of the repository
```
#!bash
git status
```

You should be seeing something like this: 

![Changes detected](http://i.imgur.com/XTKxhdZ.png?1)

The *workspace.xml* and *Forecast.java* files have changed. The work space has been automatically changed by IntelliJ and we just changed *Forecast.java*. Git knows that you've made changes to the files marked in red, but since you haven't *staged* the files yet, it won't include them in the next commit. 

##.gitignore
`.gitignore` is a file that you *can* include in every folder of your repository to hide files from git. If you look at the `.gitignore` file that we have in the base directory of the repository, you'll see that it has a single line with `out` in it. That means we're telling git to ignore the directory called *out* and any files it contains. You may want to ignore files to reduce the repository's size or just to separate developer specific data from the data meant for the remote repository. Generally you should only include source code in a repository and not developer specific data such as IntelliJ's *.idea* files, so strictly speaking the `.gitignore` file should also have a line for `.idea`, but because I like storing the work space data for my projects we're going to break that rule for now.

#The working tree and the staging area
When you've navigated into a directory that has a `.git` folder in it, you're in a git repository. The `.git` folder forms the top level of the repository and any other files or folders in that directory form the **working tree**. Git will track changes made to all the files in the working tree that have been marked for tracking. The git working tree thus contains all your checked out source code and any changes you've made. 

Changes that have been staged for commit are said to be in the staging area. That means that they will form part of the next commit. So typically you'd make changes to several files while you're working on a project until you've decided that you've finished making changes to those files and that they're ready to be included in the next commit. At that point you stage them. You don't have to wait until you're ready to commit to stage your files, you can always make further changes and then stage the new changes as well. 

##Staging changes

So suppose we're happy with that part of the code and we decide to stage our changes. You can type `git add <filename>` to stage the changes from one file at a time, or type `git add .` to stage all the changes. You should avoid using `git add .` when you're not sure what each change means, as you may end up adding code to be staged in the next commit that does not actually belong in that commit. 

We're going to stage all the changes since we know exactly why things have changed, so type 
```
#!bash
git add .
```

Now check the status of the repository again by typing 
```
#!bash
git status
```

If you didn't move your cursor in IntelliJ between the `git add .` and `git status` command you should see the output: 

![Work space unchanged](http://i.imgur.com/VNHxWLs.png?1)

Otherwise you'll see this output:

![Work space changed](http://i.imgur.com/4enrrgW.png?1)

In either case, open the IntelliJ window again, move the cursor to any arbitrary position in the file and then run `git status` again.

Git shows that the changes you added are ready for the next commit. Anything you didn't stage, like the new changes to the work space, will not be included in the commit until you've added it. Another useful tool that git has is the ability to change the working tree to a previously stored state. Git can only move to different commits, so it only tracks past states stored as commits and the changes in the current working tree. 

##Revert unstaged changes

Let's assume that I don't like the unstaged changes to the work space and that I want to change the working tree to reflect the state it was in before those changes were made. This is simple to achieve for any changes in git, **however since these changes haven't been committed yet, reverting to the previous state will mean that they will be lost**. We don't really care about these changes, so let's go ahead and revert them: 

To discard the unstaged changes, follow the instructions in git bash and type 
```
#!bash
git checkout -- .idea/workspace.xml
```

Run 
```
#!bash
git status
```

The unstaged changes to the work space have now been reverted. It isn't easy to see, but git has actually changed the content of *workspace.xml* to the state before the changes were made. 

##Unstage changes

Suppose we've decided that we don't want the staged change to *Forecast.java* to be included with the next commit. You could simply continue changing the file to reflect what you *do* want, or you could unstage the changes and revert them back to the state before change. The latter option is better if you want an *undo* of sorts to your working tree. This doesn't happen often, but we'll do it for the sake of completeness.

To unstage the changes to *Forecast.java*, follow the instructions returned from the status prompt by typing (remember to use tab completion to save time while typing)
```
#!bash
git reset HEAD simpleWeather/src/nmmu/gamedev/tut/Forecast.java
``` 

Run 
```
#!bash
git status
```

You should see that the changes have been unstaged, however we haven't reverted them yet. The changes are still there, they just won't be included in the next commit.

Discard the changes to *Forecast.java* as before by typing 
```
#!bash
git checkout -- simpleWeather/src/nmmu/gamedev/tut/Forecast.java
```

Run 
```
#!bash
git status
```

If you open IntelliJ again you'll notice that *Forecast.java* has miraculously reverted to its previous state. The bad news is that **since these changes were never committed, there is no way of getting them back.** When you're starting off with git, you should use the `git reset` and `git checkout` commands with great caution. Always double check that you're sure before running the commands as you may end up losing changes permanently.

#Committing changes

Let's manually redo the changes we reverted and then add some new ones. 

With IntelliJ open change the `display()` method in *Forecast.java* to include the text attribute and the mean temperature. Here's the code:
```
#!java
public void display() {System.out.printf("%s\tMean %.2f'C - Max %d'C - Min %d'C -- %s\n", day, ((low+high)*1.0)/2, low, high, text);}
```

Run the program to see the new results and then check the repository's status by typing
```
#!bash
git status
```

We're finally happy with the results and we're ready to commit. So firstly we'll have to stage our changes and then we'll commit them:

Run 
```
#!bash
git add .
```

Check the status of the repository with 
```
#!bash
git status
```

Now commit your changes by typing 
```
#!bash
git commit -m 'added mean temp and weather description'
```

Check the repo's status
```
#!bash
git status
```

Congratulations! You've done your first commit. As you may have noticed the syntax for the commit command takes an optional commit message `-m '<your-message-here>'`. It is good practice to include a relevant commit message with every commit you make.

Run the following to see your commit log for this repository
```
#!bash
git log
```

![Commit log](http://i.imgur.com/PATRbkH.png?1)

You're probably wondering why there are several commits when you've done only one? A repository is simply a collection of commits and since we're working with a clone of a remote repository, there will naturally be at least one commit in addition to your new one. 

#Branches and merging
You can think of branches as logical groupings of related commits in a repository (think of branches in a tree). In reality a branch in git is simply a pointer to a specific commit (see [this link](http://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell) for a very detailed explanation). 

The working tree has **at least** one *branch* called *master* where the project's stable source code is usually located. One common workflow is to create a new branch for every issue or feature you add to the project. This is advantageous as any unstable code introduced by an issue is then isolated to the new branch specifically dedicated to that issue. Once the code in the new branch is stable it is then [merged](http://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) with the main branch, implicitly updating the main source code and keeping it stable for other team members to use. We'll be employing this workflow from now on.

##Branching
We're going to add a new feature to our project and therefore we'll be creating a temporary branch to house the code until it's stable enough to be merged back into the master branch. 

You can view the branches already present in your repository by typing
```
#!bash
git branch
```
The asterisk that you should be seeing indicates the active branch.

We'll be adding a feature to store the user's local city if they choose to do so. Create the new branch for this feature by typing 
```
#!bash
git checkout -b saveCity
```

This command is actually shorthand for `git branch saveCity` followed by `git checkout saveCity`. The `git checkout` command is used to *check out* or move to a branch that isn't currently active --- think library books.

Confirm that we've created the new branch and checked it out by typing
```
#!bash
git branch
```
The asterisk should now be visible next to the *saveCity* branch.

When you check out a branch (or even a commit for that matter [remember that a branch is a pointer to a commit]), your working tree changes to reflect the commits and unstaged changes in that branch. Since we haven't actually made any changes yet there won't be any visible difference when switching between the *master* and *saveCity* branches.

##Moving unstaged changes to another branch with the stash command
You may find yourself in a situation where you've accidentally started making changes on one branch, and then realized too late that you actually wanted to commit those changes on another branch. In this case you will need to *stash* your changes, switch branches, *unstash* and then apply those changes.

[Stashing](http://git-scm.com/book/en/v1/Git-Tools-Stashing) is specifically made for *stashing* changes that haven't been commited to be worked on later. When you apply the stash command git stores your changes on a stack of uncommited changes which can be retrieved later.

To simulate this problem we're going to have to go back to the master branch:
```
#!bash
git checkout master
```

Now start making the desired changes for the new feature by added the following code

Stash your unstaged changes by typing
```
#!bash
git stash save "incomplete saveCity work"
```

You can see your saved stashes with 
```
#!bash
git stash list
```

Check the status of your repository and confirm that the working tree is now free of unstaged changes.
```
#!bash
git status
```

Now that we have a clean working tree on the master branch, we're happy to move to the branch where we want to re-apply our changes. Move to the *saveCity* branch:
```
#!bash
git checkout saveCity
```

Confirm that we're in the correct branch. The asterisk should be next to the saveCity branch.
```
#!bash
git branch
```

Now apply the **latest** stashed changes to the current branch and remove the **last** changes stashed. 
```
#!bash
git stash pop
```
This command is actually short for `git stash apply` followed by `git stash drop`.

##Merging

#Remote revisited

##Fetching data from remote repositories

##Merging branches from a remote repository

##Seeing the changes between commits

##Pulling from a remote repository

##Pushing to a remote repository

#References
1. Jae Woo Lee and Stephen A. Edwards' Git Tutorial. Available [here](http://www.cs.columbia.edu/~sedwards/classes/2013/4840/git-tutorial.pdf).
2. Github Cheat Sheet. Available [here](https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf).